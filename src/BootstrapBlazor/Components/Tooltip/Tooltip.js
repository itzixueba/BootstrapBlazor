﻿(function ($) {
    $.extend({
        bb_tooltip: function (id, method, title, placement, html, trigger) {
            var op = { html: html, sanitize: !html, title: title, placement: placement, trigger: trigger };
            var instance = bootstrap.Tooltip.getInstance(document.getElementById(id));
            if (instance) instance.dispose();
            var $ele = $('#' + id);
            if (method === "") {
                $ele.tooltip(op);
            }
            else if (method === 'enable') {
                $ele.tooltip(op);
                var $ctl = $ele.parents('form').find('.is-invalid:first');
                if ($ctl.prop("nodeName") === 'INPUT') {
                    if ($ctl.prop('readonly')) {
                        $ctl.trigger('focus');
                    }
                    else {
                        $ctl.focus();
                    }
                }
                else if ($ctl.prop("nodeName") === 'DIV') {
                    $ctl.trigger('focus');
                }
            }
            else if (method !== "dispose") {
                $ele.tooltip(op);
                $ele.tooltip(method);
            }
        },
    });

    $(function () {
        $(document)
            .on('inserted.bs.tooltip', '.is-invalid', function () {
                $('#' + $(this).attr('aria-describedby')).addClass('is-invalid');
            });
    });
})(jQuery);
